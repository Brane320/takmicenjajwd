import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, Container,Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login/Login"
import {logout} from './services/auth'
import Takmicenja from './components/takmicenja/Takmicenja';
import CreateTakmicenje from './components/takmicenja/CreateTakmicenje';
import Prijava from './components/takmicenja/Prijava';
import EditTakmicenje from './components/takmicenja/EditTakmicenje';
import Prijave from './components/takmicenja/Prijave';
import Register from './components/register/Register';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import reducers from "./reducers";



class App extends React.Component {
  

    render() {
        return (
            
          <div>
          <Router>
            <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                  Home
              </Navbar.Brand>
             
              
              <Nav>
                <Nav.Link as={Link} to="/takmicenja">
                  Prikaz takmicenja
                </Nav.Link>

                <Nav.Link as={Link} to="/prijave">
                  Prikaz prijava
                </Nav.Link>

                {window.localStorage['jwt'] ? 
                          <Button hidden onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/register">Register</Nav.Link>
                          }

                
               
                {window.localStorage['jwt'] ? 
                          <Button onClick = {()=>logout()}>Logout</Button> :
                          <Nav.Link as={Link} to="/login">Login</Nav.Link>
                          }
              </Nav>
            </Navbar>
              <Container style={{paddingTop:"25px"}}>
                <Switch>
                  
                  
                <Route exact path="/login" component={Login}/>
                <Route exact path="/takmicenja" component={Takmicenja}/>
                <Route exact path="/createtakmicenje" component={CreateTakmicenje}/>
                <Route exact path="/prijava/:id" component={Prijava}/>
                <Route exact path="/edittakmicenje/:id" component={EditTakmicenje}/>
                <Route exact path="/prijave" component={Prijave}/>
                <Route exact path="/register" component={Register}/>
                  
                  
                </Switch>
              </Container>
            </Router>
            
          </div>
        )
    }
};

let storeEnhancer = applyMiddleware(thunk);



ReactDOM.render(
  <Provider store={createStore(reducers, storeEnhancer)}>
    <App />
  </Provider>,
  document.querySelector("#root")
);