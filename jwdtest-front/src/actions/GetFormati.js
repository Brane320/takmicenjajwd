import {getFormati} from './../apis/TestAxios';

let getFormatiAction = function () {

    return async function (dispach,getState){
        try{
            let formati = await getFormati();
            dispach({type : "GET_FORMATI", payload : formati})
        }catch(error){
            dispach({type : "GET_FORMATI", payload : []})
            //Mora vratiti praznu listu mora biti prazna lista ne moze nedefinisano
        }
    }

}

export default getFormatiAction;