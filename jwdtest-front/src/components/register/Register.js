import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";

class Register extends React.Component {

    constructor(props){
        super(props);
        

    let korisnikRegistracijaDto = {
        korisnickoIme: "",
        eMail : "",
        ime : "",
        prezime : "",
        lozinka : "",
        ponovljenaLozinka : ""
    }

    this.state = {korisnikRegistracijaDto : korisnikRegistracijaDto};
}



valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let korisnikRegistracijaDto = this.state.korisnikRegistracijaDto;
    korisnikRegistracijaDto[name] = value;

    this.setState({ korisnikRegistracijaDto: korisnikRegistracijaDto });
  }

  register(e){
        
    let korisnikReg = this.state.korisnikRegistracijaDto;
    let korisnikRegistracijaDto = {
        korisnickoIme: korisnikReg.korisnickoIme,
        eMail : korisnikReg.eMail,
        ime : korisnikReg.ime,
        prezime : korisnikReg.prezime,
        lozinka : korisnikReg.lozinka,
        ponovljenaLozinka : korisnikReg.ponovljenaLozinka
        
        


    }
    
      

   
    TestAxios.post('/korisnici' , korisnikRegistracijaDto)
    .then(res => {
        console.log(res);

        alert('Uspesna registracija');
        this.props.history.push("/login");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesna registracija');
    });
 
}


render() {
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
            <Form.Label htmlFor="zkorisnicko">Korisnicko ime</Form.Label>
            <Form.Control id="zkorisnicko" name="korisnickoIme" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zmail">Email</Form.Label>
            <Form.Control id="zmail" name="eMail" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zime">Ime</Form.Label>
            <Form.Control id="zime" name="ime" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zprezime">Prezime</Form.Label>
            <Form.Control id="zprezime" name="prezime" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zlozinka">Lozinka</Form.Label>
            <Form.Control type="password" id="zlozinka" name="lozinka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label  id="zponovljenalozinka">Ponovljena lozinka</Form.Label>
            <Form.Control type="password" id="zponovljenalozinka" name="ponovljenaLozinka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>


           
            

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.register(e)}>Registruj se</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    );

   }

}












export default Register;