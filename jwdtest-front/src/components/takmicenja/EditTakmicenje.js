import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import getFormatiAction from '../../actions/GetFormati';
import { connect } from "react-redux";
class EditTakmicenje extends React.Component {

    constructor(props){
        super(props);

    let takmicenje = {
        id:-1,
        naziv: "",
        mestoOdrzavanja : "",
        datumPocetka : "",
        datumZavrsetka : "",
        format: {}
    }

    this.state = {takmicenje : takmicenje, formati : []};
}

componentDidMount(){
    this.props.getFormati();
    this.getTakmicenjeById(this.props.match.params.id);
    
 

}

getFormati(){
    TestAxios.get('/formati')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({formati: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa ocitavanjem sprintova!');
        });
}

getTakmicenjeById(takmicenjeId) {
    TestAxios.get('/takmicenja/' + takmicenjeId)
    .then(res => {
        // handle success
        console.log(res.data);
        let takmicenje = {
            id : res.data.id,
            naziv: res.data.naziv,
            mestoOdrzavanja : res.data.mestoOdrzavanja,
            datumPocetka : res.data.datumPocetka,
            datumZavrsetka : res.data.datumZavrsetka,
            format: res.data.format
        }
        this.setState({takmicenje : takmicenje});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Takmicenje nije dobijeno')
     });
}

formatSelectionChanged(e){
    // console.log(e);

    let formatId = e.target.value;
    let format = this.props.formati.find((formatDTO) => formatDTO.id == formatId);
    console.log(format);

    let takmicenje = this.state.takmicenje;
    takmicenje.format = format;
    this.setState({takmicenje : takmicenje});
   
   
}

renderFormatOptions() {
    return this.state.formati.map(format => {
        return (
            <option key={format.id} value={format.id}>
                {format.tip}
            </option>
        )
    });
}


valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let takmicenje = this.state.takmicenje;
    takmicenje[name] = value;

    this.setState({ takmicenje: takmicenje });
  }

  renderFormatOptions() {
    return this.props.formati.map(format => {
        return (
            <option key={format.id} value={format.id}>
                {format.tip}
            </option>
        )
    });
}

edit(){
    let takmicenje = this.state.takmicenje;
    let takmicenjeDTO = {
        id:takmicenje.id,
        naziv: takmicenje.naziv,
        mestoOdrzavanja : takmicenje.mestoOdrzavanja,
        datumPocetka : takmicenje.datumPocetka,
        datumZavrsetka : takmicenje.datumZavrsetka,
        format: takmicenje.format

        
        


    }
    TestAxios.put('/takmicenja/' + this.state.takmicenje.id, takmicenjeDTO)
    .then(res => {
        console.log(res);

        alert('Zadatak has been edited sucessfully!');
        this.props.history.push("/takmicenja");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno editovanje takmicenja!');
    });
}

render(){
    return (
        <>
        <Row>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
            <Form.Label htmlFor="znaziv">Naziv takmicenja</Form.Label>
            <Form.Control id="znaziv" value={this.state.takmicenje.naziv} name="naziv" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label htmlFor="zmesto">Mesto odrzavanja</Form.Label>
            <Form.Control id="zmesto" value={this.state.takmicenje.mestoOdrzavanja} name="mestoOdrzavanja" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zpocetak">Datum pocetka takmicenja</Form.Label>
            <Form.Control type="date" value={this.state.takmicenje.datumPocetka} id="zpocetak" name="datumPocetka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            <Form.Label id="zzavrsetak">Datum zavrsetka takmicenja</Form.Label>
            <Form.Control type="date" value={this.state.takmicenje.datumZavrsetka} id="zzavrsetak" name="datumZavrsetka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>

            <Form.Label htmlFor="pformat">Format</Form.Label>
            <InputGroup>
            <Form.Control as="select" value={this.state.takmicenje.format.id} id="pformat" name="format" onChange={(e)=>this.formatSelectionChanged(e)}>
           
            {this.renderFormatOptions()}
            </Form.Control>

            </InputGroup>
            

       
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.edit(e)}>Izmeni takmicenje</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}

}


const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    return { formati: state.formati };
  };
  
  export default connect(mapStateToProps, {
    getFormati : getFormatiAction
  })(EditTakmicenje);