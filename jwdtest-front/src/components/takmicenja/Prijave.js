import React from 'react';
import {Table, Button,Col,Form,InputGroup} from 'react-bootstrap'
import TestAxios from '../../apis/TestAxios';

class Prijave extends React.Component {

    constructor(props) {
        super(props);

        this.state = { prijave : [],brojStranice : 0,totalPages : 0}
    }
    

    componentDidMount(){
        this.getPrijave(0);
    }

    getPrijave(brojStranice) {
        let config = {
            params: {
              brojStranice: this.state.brojStranice,
              brojStranice : brojStranice
            },
          }

        TestAxios.get('/prijave', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({prijave: res.data,
                totalPages : res.headers["total-pages"],
                brojStranice : brojStranice
            });
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri ocitavanju prijava');
            });
    }

    renderPrijave() {
        return this.state.prijave.map((prijava) => {
            return (
               <tr key={prijava.id}>
                  <td>{prijava.drzava}</td>
                  <td>{prijava.kontakt}</td>
                  <td>{prijava.datumPrijave}</td>
                  <td>{prijava.takmicenje.naziv}</td>
                  
                  
               
               </tr>
            )
         })
    }

    render() {
       
     
       
        
        return (
            
            <div>
               
                
                
                
                <div>
                   
                    <br/>
                    <div style={{float:"right"}}><Button disabled={this.state.brojStranice==0} onClick={()=>this.getPrijave(this.state.brojStranice -1)}>Prethodna</Button>
                  
                  <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getPrijave(this.state.brojStranice+1)}>Sledeca</Button>
              </div>
                    
                    <Table style={{marginTop:5}}>
                        <thead className="thead-dark">
                            <tr>
                                <th>Drzava</th>
                                <th>Kontakt</th>
                                <th>Datum prijave</th>
                                <th>Takmicenje</th>
                                
                        
                                
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderPrijave()}
                        </tbody>                  
                    </Table>
                    
                </div>
              

            </div>
        );
    }



}
export default Prijave;