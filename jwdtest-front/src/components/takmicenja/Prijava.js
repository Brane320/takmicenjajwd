import React from 'react';
import TestAxios from '../../apis/TestAxios';
import {Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class Prijava extends React.Component {

    constructor(props){
        super(props);
        let takmicenje = {
            id : -1,
            naziv: "",
            mestoOdrzavanja : "",
            datumPocetka : "",
            datumZavrsetka : "",
            format: {}
        }

    this.state = {id : 0, takmicenje: takmicenje, kontakt : "", drzava : ""};
}
componentDidMount(){
    this.getTakmicenjeById(this.props.match.params.id);
    
 
}

getTakmicenjeById(takmicenjeId) {
    TestAxios.get('/takmicenja/' + takmicenjeId)
    .then(res => {
        // handle success
        console.log(res.data);
        let takmicenje = {
            id : res.data.id,
            naziv: res.data.naziv,
            mestoOdrzavanja : res.data.mestoOdrzavanja,
            datumPocetka : res.data.datumPocetka,
            datumZavrsetka : res.data.datumZavrsetka,
            format: res.data.format
        }
        this.setState({takmicenje : takmicenje});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Takmicenje nije dobijeno')
     });
}

kontaktInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let kontakt = this.state.kontakt;
    kontakt = value;

    this.setState({ kontakt : kontakt});
  }

  drzavaInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let drzava = this.state.drzava;
    drzava = value;

    this.setState({ drzava : drzava});
  }

  prijava(e){
        
    
        
    let prijavaDTO = {
        kontakt : this.state.kontakt,
        drzava : this.state.drzava,
        takmicenje : this.state.takmicenje,

        


    }

   
    TestAxios.post('/prijave' , prijavaDTO)
    .then(res => {
        console.log(res);

        alert('Uspesna prijava');
        this.props.history.push("/prijave");
    
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesna prijava');
    });
}

render(){
    return (
        <>
        <Row>
            <h1>Prijava na takmicenje</h1>
        <Col></Col>
        <Col xs="14" sm="12" md="12">
        <Form>
          
            <Form.Label id="zkontakt">Kontakt</Form.Label>
            <Form.Control id="zkontakt" name="kontakt" onChange={(e)=>this.kontaktInputChanged(e)}/> <br/>
            <Form.Label id="zdrzava">Drzava</Form.Label>
            <Form.Control id="zdrzava" name="drzava" onChange={(e)=>this.drzavaInputChanged(e)}/> <br/>
            <Form.Label id="ztakmicenje">Takmicenje</Form.Label>
            <Form.Control id="ztakmicenje" name="takmicenje" readOnly value = {this.state.takmicenje.naziv}  /> <br/>

           

            <br/>
            
           
     
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.prijava(e)}>Prijavi se</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>


      
 
    </>

    

    )
}




}

export default Prijava;