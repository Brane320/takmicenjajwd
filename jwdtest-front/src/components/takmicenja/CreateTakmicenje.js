import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import getFormatiAction from '../../actions/GetFormati';
import { connect } from "react-redux";
class CreateTakmicenje extends React.Component {

    constructor(props){
        super(props);

    let takmicenje = {
        naziv: "",
        mestoOdrzavanja : "",
        datumPocetka : "",
        datumZavrsetka : "",
        format: null
    }

    this.state = {takmicenje: takmicenje, formati : []};
}
    create = this.create.bind(this);

    componentDidMount(){
       this.props.getFormati();
    }

    getFormati(){
        TestAxios.get('/formati')
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({formati: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Problem sa ocitavanjem sprintova!');
            });
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);
    
        let takmicenje = this.state.takmicenje;
        takmicenje[name] = value;
    
        this.setState({ takmicenje: takmicenje });
      }

      formatSelectionChanged(e){
        // console.log(e);

        let formatId = e.target.value;
        let format = this.props.formati.find((formatDTO) => formatDTO.id == formatId);
        console.log(format);

        let takmicenje = this.state.takmicenje;
        takmicenje.format = format;
        this.setState({takmicenje : takmicenje});
       
       
    }

    renderFormatOptions() {
        return this.props.formati.map(format => {
            return (
                <option key={format.id} value={format.id}>
                    {format.tip}
                </option>
            )
        });
    }

    create(e){
        
        let takmicenje = this.state.takmicenje;
        let takmicenjeDTO = {
            naziv: takmicenje.naziv,
            mestoOdrzavanja : takmicenje.mestoOdrzavanja,
            datumPocetka : takmicenje.datumPocetka,
            datumZavrsetka : takmicenje.datumZavrsetka,
            format: takmicenje.format
            


        }

       
        TestAxios.post('/takmicenja' , takmicenjeDTO)
        .then(res => {
            console.log(res);

            alert('Takmicenje has been added sucessfully!');
            this.props.history.push("/takmicenja");
        })

        .catch(error => {
            console.log(error);

            alert('Error! Neuspesno kreiranje takmicenja');
        });
    }

    render(){
        return (
            <>
            <Row>
            <Col></Col>
            <Col xs="14" sm="12" md="12">
            <Form>
                <Form.Label htmlFor="znaziv">Naziv takmicenja</Form.Label>
                <Form.Control id="znaziv" name="naziv" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
                <Form.Label htmlFor="zmesto">Mesto odrzavanja</Form.Label>
                <Form.Control id="zmesto" name="mestoOdrzavanja" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
                <Form.Label id="zpocetak">Datum pocetka takmicenja</Form.Label>
                <Form.Control type="date" id="zpocetak" name="datumPocetka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
                <Form.Label id="zzavrsetak">Datum zavrsetka takmicenja</Form.Label>
                <Form.Control type="date" id="zzavrsetak" name="datumZavrsetka" onChange={(e)=>this.valueInputChanged(e)}/> <br/>

                <Form.Label htmlFor="pformat">Format</Form.Label>
                <InputGroup>
                <Form.Control as="select" id="pformat" name="format" onChange={(e)=>this.formatSelectionChanged(e)}>
               
                {this.renderFormatOptions()}
                </Form.Control>

                </InputGroup>
                

           
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e)}>Kreiraj takmicenje</Button>
            </Form>
            </Col>

            
            <Col></Col>
            </Row>

    
          
     
        </>

        

        )
    }

}

const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    return { formati: state.formati };
  };
  
  export default connect(mapStateToProps, {
    getFormati : getFormatiAction
  })(CreateTakmicenje);
