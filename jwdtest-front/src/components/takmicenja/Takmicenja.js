import React from 'react';
import {Table, Button,Col,Form,InputGroup} from 'react-bootstrap'
import TestAxios from '../../apis/TestAxios';
import getFormatiAction from '../../actions/GetFormati';
import { connect } from "react-redux";

class Takmicenja extends React.Component {

    constructor(props) {
        super(props);

        this.state = { takmicenja: [],formati:[],brojStranice : 0,totalPages : 0, formatId : null, mestoOdrzavanja : null}
    }
    

    componentDidMount(){
        this.getTakmicenja(0);
      //  this.getFormati();
        this.provera();
        this.props.getFormati();
    }

    
    mestoInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;

        console.log(name + ", " + value);

        let mestoOdrzavanja = this.state.mestoOdrzavanja;

        mestoOdrzavanja = value;
        

        this.setState({mestoOdrzavanja : mestoOdrzavanja})

        
    
   
        console.log(mestoOdrzavanja);
      }

      provera(){
        {window.localStorage['jwt'] ? 
      this.render() :
      this.props.history.push('/login')
      }
    }
    

      getFormati(){
        TestAxios.get('/formati')
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({formati: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Problem sa ocitavanjem formata!');
            });
    }
      
      formatelectionChanged(e){
        // console.log(e);

        let formatId = e.target.value;
        let format = this.state.formati.find((formatDTO) => formatDTO.id == formatId);
        console.log(format);
        this.setState({formatId : format.id});
    
        
    
       
       
    }

    renderFormatOptions() {
        return this.props.formati.map(format => {
            return (
                <option key={format.id} value={format.id}>
                    {format.tip}
                </option>
            )
        });
    }
    pretraga() {
        var params = {
            'formatId' : this.state.formatId,
            'mestoOdrzavanja' : this.state.mestoOdrzavanja


        }

        TestAxios.get('/takmicenja/pretraga', {params})
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({takmicenja : res.data});
                 
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri pretrazi');
            });
    }
    goToAdd(){
        this.props.history.push('/createtakmicenje');
    }



    getTakmicenja(brojStranice) {
        let config = {
            params: {
              brojStranice: this.state.brojStranice,
              brojStranice : brojStranice
            },
          }

        TestAxios.get('/takmicenja', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({takmicenja: res.data,
                totalPages : res.headers["total-pages"],
                brojStranice : brojStranice
            });
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri ocitavanju takmicenja');
            });
    }

    prijava(takmicenjeId) {
        this.props.history.push('/prijava/' + takmicenjeId); 
    }

    
    goToEdit(takmicenjeId) {
        this.props.history.push('/edittakmicenje/' + takmicenjeId); 
    }
    delete(id) {
        TestAxios.delete('/takmicenja/' + id)
        .then(res => {
            // handle success
            console.log(res);
            alert('Takmicenje uspesno obrisano');
            window.location.reload();
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa brisanjem ');
         });
    }


    

    renderTakmicenja() {
        return this.state.takmicenja.map((takmicenje) => {
            return (
               <tr key={takmicenje.id}>
                  <td>{takmicenje.naziv}</td>
                  <td>{takmicenje.mestoOdrzavanja}</td>
                  <td>{takmicenje.datumPocetka}</td>
                  <td>{takmicenje.datumZavrsetka}</td>
                  <td>{takmicenje.format.tip}</td>
                  <td> 
                  <Button hidden={takmicenje.format.brojUcesnika == 0} onClick={() => this.prijava(takmicenje.id)}>Prijavi se</Button>
                  <Button style={{margin:3}} variant="danger" onClick={() => this.delete(takmicenje.id)}>Brisanje</Button>
                  <Button style={{margin:3}}  onClick={() => this.goToEdit(takmicenje.id)}>Izmena</Button></td>

                  
               </tr>
            )
         })
    }

    render() {
       
     
       
        
        return (
            
            <div>
                 <div>
           
        
            <Col></Col>
            <Col xs="14" sm="12" md="12">
            <Form>
            <Form.Label htmlFor="formatt">Format</Form.Label>
                <InputGroup>
                <Form.Control as="select" id="formatt" name="format" onChange={e => this.formatelectionChanged(e)}>
                 {this.renderFormatOptions()}
                </Form.Control>
                <br/>
               
                </InputGroup>
                <Form.Label htmlFor="im">Mesto odrzavanja</Form.Label>
                <Form.Control id="im" name="mestoOdrzavanja" onChange={(e)=>this.mestoInputChanged(e)}/> <br/>

                
               
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.pretraga()}>Pretrazi</Button>
            </Form>
            </Col>
        </div>
                
                
                
                <div>
                    <Button class="btn btn-success" onClick={() => this.goToAdd()} variant="warning">Kreiraj takmicenje</Button>
                    <br/>
                    <div style={{float:"right"}}><Button disabled={this.state.brojStranice==0} onClick={()=>this.getTakmicenja(this.state.brojStranice -1)}>Prethodna</Button>
                  
                  <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getTakmicenja(this.state.brojStranice+1)}>Sledeca</Button>
              </div>
                    
                    <Table style={{marginTop:5}}>
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv takmicenja</th>
                                <th>Mesto odrzavanja</th>
                                <th>Datum pocetka takmicenja</th>
                                <th>Datum zavrsetka takmicenja</th>
                                <th>Format</th>
                                <th></th>
                        
                                
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTakmicenja()}
                        </tbody>                  
                    </Table>
                    
                </div>
              

            </div>
        );
    }

}



const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    return { formati: state.formati };
  };
  
  export default connect(mapStateToProps, {
    getFormati : getFormatiAction
  })(Takmicenja);