package test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Prijava;

@Repository
public interface PrijavaRepository extends JpaRepository<Prijava,Long>{

}
