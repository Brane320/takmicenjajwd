package test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format,Long>{

}
