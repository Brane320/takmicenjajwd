package test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Takmicenje;

@Repository
public interface TakmicenjeRepository extends JpaRepository<Takmicenje,Long>{
	
	List<Takmicenje> findByFormatIdAndMestoOdrzavanjaIgnoreCaseContains(Long formatId,String mestoOdrzavanja);
	 
	List<Takmicenje> findByMestoOdrzavanjaIgnoreCaseContains(String mestoOdrzavanja);
}
