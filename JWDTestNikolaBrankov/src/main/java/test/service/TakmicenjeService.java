package test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import test.model.Takmicenje;

public interface TakmicenjeService {
	
	Page<Takmicenje> findAll(int brojStranice);
	
	Takmicenje findOneById(Long id);
	
	Optional<Takmicenje> getById(Long id);
	
	Takmicenje save(Takmicenje takmicenje);
	
	Takmicenje update(Takmicenje takmicenje);
	
	Takmicenje delete(Long id);
	
	List<Takmicenje> find(Long formatId,String mestoOdrzavanja);
	
	

}
