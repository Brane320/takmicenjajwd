package test.service;

import java.util.List;

import test.model.Format;

public interface FormatService {
	
	List<Format> findAll();
	
	Format findOneById(Long id);
	
	Format update(Format format);

}
