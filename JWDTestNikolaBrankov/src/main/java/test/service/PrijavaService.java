package test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import test.model.Prijava;

public interface PrijavaService {
	Prijava save(Prijava prijava);
	
	Prijava findOneById(Long id);
	
	List<Prijava> findAll();
	
	Page<Prijava> findAll(int brojStranice);

}
