package test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import test.model.Takmicenje;
import test.repository.TakmicenjeRepository;
import test.service.TakmicenjeService;

@Service
public class JpaTakmicenjeService implements TakmicenjeService{
	
	@Autowired
	private TakmicenjeRepository takmicenjeRepository;

	@Override
	public Page<Takmicenje> findAll(int brojStranice) {
		return takmicenjeRepository.findAll(PageRequest.of(brojStranice, 3));
	}

	@Override
	public Takmicenje findOneById(Long id) {
		return takmicenjeRepository.getOne(id);
	}

	@Override
	public Optional<Takmicenje> getById(Long id) {
		return takmicenjeRepository.findById(id);
	}

	@Override
	public Takmicenje save(Takmicenje takmicenje) {
		return takmicenjeRepository.save(takmicenje);
	}

	@Override
	public Takmicenje update(Takmicenje takmicenje) {
		return takmicenjeRepository.save(takmicenje);
	}

	@Override
	public Takmicenje delete(Long id) {
		Optional<Takmicenje> takmicenje = takmicenjeRepository.findById(id);
		
		if(takmicenje.isPresent()) {
			takmicenjeRepository.deleteById(id);
			return takmicenje.get();
		}
		return null;
	}

	@Override
	public List<Takmicenje> find(Long formatId, String mestoOdrzavanja) {
		if(formatId == null && mestoOdrzavanja == null) {
			return takmicenjeRepository.findAll();
		}
		if(formatId == null) {
			return takmicenjeRepository.findByMestoOdrzavanjaIgnoreCaseContains(mestoOdrzavanja);
		}
		
		if(mestoOdrzavanja == null) {
			mestoOdrzavanja = "";
		}
		
		return takmicenjeRepository.findByFormatIdAndMestoOdrzavanjaIgnoreCaseContains(formatId, mestoOdrzavanja);
	}

}
