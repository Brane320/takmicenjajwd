package test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.model.Format;
import test.repository.FormatRepository;
import test.service.FormatService;
@Service
public class JpaFormatService implements FormatService {
	
	@Autowired
	private FormatRepository formatRepository;

	@Override
	public List<Format> findAll() {
		return formatRepository.findAll();
	}

	@Override
	public Format findOneById(Long id) {
		return formatRepository.getOne(id);
	}

	@Override
	public Format update(Format format) {
		return formatRepository.save(format);
	}

}
