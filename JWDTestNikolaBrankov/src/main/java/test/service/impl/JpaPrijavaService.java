package test.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import test.model.Format;
import test.model.Prijava;
import test.repository.PrijavaRepository;
import test.service.FormatService;
import test.service.PrijavaService;
@Service
public class JpaPrijavaService implements PrijavaService {
	
	@Autowired
	private PrijavaRepository prijavaRepository;
	
	@Autowired
	private FormatService formatService;

	@Override
	public Prijava save(Prijava prijava) {
		Format format = prijava.getTakmicenje().getFormat();
		format.setBrojUcesnika(format.getBrojUcesnika() - 1);
		formatService.update(format);
		
		prijava.setDatumPrijave(LocalDate.now().toString());
		return prijavaRepository.save(prijava);
	}

	@Override
	public Prijava findOneById(Long id) {
		return prijavaRepository.getOne(id);
	}

	@Override
	public List<Prijava> findAll() {
		return prijavaRepository.findAll();
	}

	@Override
	public Page<Prijava> findAll(int brojStranice) {
		return prijavaRepository.findAll(PageRequest.of(brojStranice, 3));
	}

}
