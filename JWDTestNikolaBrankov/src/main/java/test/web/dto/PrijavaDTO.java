package test.web.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

public class PrijavaDTO {
	
	
    private Long id;
	
    @Size(min= 3,max = 3)
	private String drzava;

	private String kontakt;

	private String datumPrijave;
	
	
	private TakmicenjeDTO takmicenje;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDrzava() {
		return drzava;
	}


	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}


	public String getKontakt() {
		return kontakt;
	}


	public void setKontakt(String kontakt) {
		this.kontakt = kontakt;
	}


	public String getDatumPrijave() {
		return datumPrijave;
	}


	public void setDatumPrijave(String datumPrijave) {
		this.datumPrijave = datumPrijave;
	}


	public TakmicenjeDTO getTakmicenje() {
		return takmicenje;
	}


	public void setTakmicenje(TakmicenjeDTO takmicenje) {
		this.takmicenje = takmicenje;
	}
	
	

}
