package test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.model.Format;
import test.model.Prijava;
import test.model.Takmicenje;
import test.service.PrijavaService;
import test.support.PrijavaDTOToPrijava;
import test.support.PrijavaToPrijavaDTO;
import test.web.dto.FormatDTO;
import test.web.dto.PrijavaDTO;
import test.web.dto.TakmicenjeDTO;

@RestController
@RequestMapping(value = "/api/prijave", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrijavaController {
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private PrijavaToPrijavaDTO toPrijavaDTO;
	
	@Autowired
	private PrijavaDTOToPrijava toPrijava;
	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PrijavaDTO> create(@Valid @RequestBody PrijavaDTO prijavaDTO){
        Prijava prijava = toPrijava.convert(prijavaDTO);
        
        Prijava sacuvana = prijavaService.save(prijava);

        return new ResponseEntity<>(toPrijavaDTO.convert(sacuvana), HttpStatus.CREATED);
    }
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	 @GetMapping
		public ResponseEntity<List<PrijavaDTO>> getAll(@RequestParam(defaultValue="0") int brojStranice){
			Page<Prijava> sve = prijavaService.findAll(brojStranice);
			HttpHeaders headers = new HttpHeaders();
			headers.set("Total-Pages", sve.getTotalPages() + "");
			
			return new ResponseEntity<>(toPrijavaDTO.convert(sve.getContent()),headers,HttpStatus.OK);
			
		}
	
	

}
