package test.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.model.Takmicenje;
import test.service.TakmicenjeService;
import test.support.TakmicenjeDTOToTakmicenje;
import test.support.TakmicenjeToTakmicenjeDTO;
import test.web.dto.TakmicenjeDTO;







@RestController
@RequestMapping(value = "/api/takmicenja", produces = MediaType.APPLICATION_JSON_VALUE)
public class TakmicenjeController {
	
	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Autowired
	private TakmicenjeToTakmicenjeDTO toTakmicenjeDTO;
	
	@Autowired
	private TakmicenjeDTOToTakmicenje toTakmicenje;
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	 @GetMapping
		public ResponseEntity<List<TakmicenjeDTO>> getAll(@RequestParam(defaultValue="0") int brojStranice){
			Page<Takmicenje> svi = takmicenjeService.findAll(brojStranice);
			HttpHeaders headers = new HttpHeaders();
			headers.set("Total-Pages", svi.getTotalPages() + "");
			
			return new ResponseEntity<>(toTakmicenjeDTO.convert(svi.getContent()),headers,HttpStatus.OK);
			
		}
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	 @GetMapping(value="/{id}")
		public ResponseEntity<TakmicenjeDTO> getById(@PathVariable Long id){
			Optional<Takmicenje> takmicenje = takmicenjeService.getById(id);
			
			if(takmicenje.isPresent()) {
				return new ResponseEntity<>(toTakmicenjeDTO.convert(takmicenje.get()),HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
		}
	 
	@PreAuthorize("hasRole('ADMIN')")
	 @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<TakmicenjeDTO> create(@Valid @RequestBody TakmicenjeDTO takmicenjeDTO){
	        Takmicenje takmicenje = toTakmicenje.convert(takmicenjeDTO);
	        Takmicenje sacuvano = takmicenjeService.save(takmicenje);

	        return new ResponseEntity<>(toTakmicenjeDTO.convert(sacuvano), HttpStatus.CREATED);
	    }
	 
	@PreAuthorize("hasRole('ADMIN')")
	 @PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<TakmicenjeDTO> update(@PathVariable Long id, @Valid @RequestBody TakmicenjeDTO takmicenjeDTO){

	        if(!id.equals(takmicenjeDTO.getId())) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }

	        Takmicenje takmicenje = toTakmicenje.convert(takmicenjeDTO);
	        Takmicenje izmenjeno = takmicenjeService.update(takmicenje);

	        return new ResponseEntity<>(toTakmicenjeDTO.convert(izmenjeno),HttpStatus.OK);
	    }
	  
	@PreAuthorize("hasRole('ADMIN')")
	  @DeleteMapping("/{id}")
			public ResponseEntity<TakmicenjeDTO> delete(@PathVariable Long id){
				Takmicenje obrisano = takmicenjeService.delete(id);
				
				if(obrisano != null) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}else {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
				
			}
	  
	  @GetMapping("/pretraga")
		public ResponseEntity<List<TakmicenjeDTO>> pretraga(@RequestParam(required=false) Long formatId, @RequestParam(required=false) String mestoOdrzavanja){
			List<Takmicenje> rezultat = takmicenjeService.find(formatId, mestoOdrzavanja);
			
			return new ResponseEntity<>(toTakmicenjeDTO.convert(rezultat),HttpStatus.OK);
		}
	

}
