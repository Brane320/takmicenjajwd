package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.model.Prijava;
import test.web.dto.PrijavaDTO;

@Component
public class PrijavaToPrijavaDTO implements Converter<Prijava,PrijavaDTO>{
	
	@Autowired
	private TakmicenjeToTakmicenjeDTO toTakmicenjeDTO;

	@Override
	public PrijavaDTO convert(Prijava source) {
		PrijavaDTO dto = new PrijavaDTO();
		
		dto.setId(source.getId());
		dto.setDrzava(source.getDrzava());
		dto.setDatumPrijave(source.getDatumPrijave());
		dto.setKontakt(source.getKontakt());
		dto.setTakmicenje(toTakmicenjeDTO.convert(source.getTakmicenje()));
		
		return dto;
	}
	
	public List<PrijavaDTO> convert(List<Prijava> prijave){
		List<PrijavaDTO> dto = new ArrayList<>();
		
		for(Prijava prijava : prijave) {
			dto.add(convert(prijava));
		}
		return dto;
	}
	

}
