package test.support;







import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.model.Format;
import test.web.dto.FormatDTO;

@Component
public class FormatToFormatDTO implements Converter<Format,FormatDTO>{

	@Override
	public FormatDTO convert(Format source) {
		FormatDTO dto = new FormatDTO();
		
		dto.setId(source.getId());
		dto.setTip(source.getTip());
		dto.setBrojUcesnika(source.getBrojUcesnika());
		
		return dto;
	}
	
	public List<FormatDTO> convert(List<Format> formati){
		List<FormatDTO> dto = new ArrayList<>();
		
		for(Format format : formati) {
			dto.add(convert(format));
		}
		return dto;
		
	}

	


}
