package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.model.Takmicenje;
import test.web.dto.TakmicenjeDTO;

@Component
public class TakmicenjeToTakmicenjeDTO implements Converter<Takmicenje,TakmicenjeDTO>{
	
	@Autowired
	private FormatToFormatDTO toFormatDTO;

	@Override
	public TakmicenjeDTO convert(Takmicenje source) {
		TakmicenjeDTO dto = new TakmicenjeDTO();
		
		dto.setId(source.getId());
		dto.setDatumPocetka(source.getDatumPocetka());
		dto.setDatumZavrsetka(source.getDatumZavrsetka());
		dto.setFormat(toFormatDTO.convert(source.getFormat()));
		dto.setMestoOdrzavanja(source.getMestoOdrzavanja());
		dto.setNaziv(source.getNaziv());
		
		return dto;
	}
	
	public List<TakmicenjeDTO> convert(List<Takmicenje> takmicenja){
		List<TakmicenjeDTO> dto = new ArrayList<>();
		
		for(Takmicenje takmicenje : takmicenja) {
			dto.add(convert(takmicenje));
		}
		return dto;
	}

}
