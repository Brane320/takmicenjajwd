package test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.model.Prijava;
import test.service.PrijavaService;
import test.service.TakmicenjeService;
import test.web.dto.PrijavaDTO;

@Component
public class PrijavaDTOToPrijava implements Converter<PrijavaDTO,Prijava> {
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private TakmicenjeService takmicenjeService;

	@Override
	public Prijava convert(PrijavaDTO dto) {
		Prijava prijava;
		
		if(dto.getId() == null) {
			prijava = new Prijava();
		} else {
			prijava = prijavaService.findOneById(dto.getId());
		}
		if(prijava != null) {
			prijava.setDatumPrijave(dto.getDatumPrijave());
			prijava.setDrzava(dto.getDrzava());
			prijava.setKontakt(dto.getKontakt());
			prijava.setTakmicenje(takmicenjeService.findOneById(dto.getTakmicenje().getId()));
		}
		return prijava;
	}

}
