package test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.model.Takmicenje;
import test.service.FormatService;
import test.service.TakmicenjeService;
import test.web.dto.TakmicenjeDTO;

@Component
public class TakmicenjeDTOToTakmicenje implements Converter<TakmicenjeDTO,Takmicenje>{
	
	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Autowired
	private FormatService formatService;

	@Override
	public Takmicenje convert(TakmicenjeDTO dto) {
		Takmicenje takmicenje;
		
		if(dto.getId() == null) {
			takmicenje = new Takmicenje();
		} else {
			takmicenje = takmicenjeService.findOneById(dto.getId());
		}
		if(takmicenje != null) {
			takmicenje.setDatumPocetka(dto.getDatumPocetka());
			takmicenje.setDatumZavrsetka(dto.getDatumZavrsetka());
			takmicenje.setFormat(formatService.findOneById(dto.getFormat().getId()));
			takmicenje.setMestoOdrzavanja(dto.getMestoOdrzavanja());
			takmicenje.setNaziv(dto.getNaziv());
		
		}
		return takmicenje;
			
	}

}
