

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
              
INSERT INTO format(id,broj_ucesnika,tip) VALUES (1, 4, 'Grand slam');
INSERT INTO format(id,broj_ucesnika,tip) VALUES (2, 6, 'Masters 1000');
INSERT INTO format(id,broj_ucesnika,tip) VALUES (3, 6, 'Masters 500');
INSERT INTO format(id,broj_ucesnika,tip) VALUES (4, 8, 'Masters 250');

INSERT INTO takmicenje(id,datum_pocetka,datum_zavrsetka,mesto_odrzavanja,naziv,format_id) VALUES (1,'22.12.2012','28.12.2012','Paris','Turnir1', 1);
INSERT INTO takmicenje(id,datum_pocetka,datum_zavrsetka,mesto_odrzavanja,naziv,format_id) VALUES (2,'22.08.2012','28.08.2012','Monaco','Turnir2', 3);
INSERT INTO takmicenje(id,datum_pocetka,datum_zavrsetka,mesto_odrzavanja,naziv,format_id) VALUES (3,'21.01.2012','23.01.2012','Vegas','Turnir3', 2);
INSERT INTO takmicenje(id,datum_pocetka,datum_zavrsetka,mesto_odrzavanja,naziv,format_id) VALUES (4,'22.04.2012','23.04.2012','London','Turnir4', 4);

INSERT INTO prijava(id,datum_prijave,drzava,kontakt,takmicenje_id) VALUES (1,'25.12.2015', 'SRB','test@gmail.com', 1);
INSERT INTO prijava(id,datum_prijave,drzava,kontakt,takmicenje_id) VALUES (2,'22.05.2013', 'RUS','test2@gmail.com', 2);
INSERT INTO prijava(id,datum_prijave,drzava,kontakt,takmicenje_id) VALUES (3,'23.08.2020', 'MNE','test3@gmail.com', 3);